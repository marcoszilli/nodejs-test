const { Router } = require('express');

const controller = require('../controller/products');

const router = Router();

router.route('/')
  .get((req, res) => controller
    .getProducts()
    .then(products => res.status(200).send(products))
    .catch(() => next())
  );

module.exports = router;
