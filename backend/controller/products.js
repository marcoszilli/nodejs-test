const Database = require('../config/database');

const { models: { Product } } = Database();

module.exports = {
  getProducts() {
    return Product.findAll()
  }
};
