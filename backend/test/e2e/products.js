const supertest = require('supertest');
const { expect } = require('chai');

const app = require('../../app');

const request = supertest(app);

describe('Products api', function () {
  before(function () {
    app.get('datasource').sequelize.sync();
  });

  it('GET /products returns an array of products', function () {
    request
      .get('/products')
      .expect(res => expect(res.body).to.be.object('array'));
  });

  it('POST /products create and return a product', function () {
    request
      .post('/products', {
        title: 'derp',
        description: 'herp',
        imageSrc: 'https://www.mensagens10.com.br/wp-content/uploads/2013/11/nem-sua-mae-te-liga.jpg',
        discount: 100,
        price: 100
      })
      .expect(res => expect(res.body).to.be.object('array'));
  });
});
