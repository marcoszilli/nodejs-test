module.exports = function (sequelize, DataTypes) {
  return sequelize
    .define(
      'Product', {
        title: {
          type: DataTypes.STRING,
        },
        description: {
          type: DataTypes.STRING,
        },
        imageSrc: {
          type: DataTypes.STRING,
        },
        discount: {
          type: DataTypes.INTEGER,
        },
        price: {
          type: DataTypes.DOUBLE,
        },
      },
      {
        freezeTableName: true,
        paranoid: true,
      },
    );
};
