import React from 'react';
import styled from 'styled-components';
import axios from 'axios';
import ListedIten from './listed-iten';

export default class ItemList extends React.Component {

  constructor() {
    super();
    this.state = {
      products: [],
    };
  }

  async componentWillMount() {
    const products = await axios.get('http://localhost:3000/products')
    .then(res => res.data)
    .catch(() => []);

    this.setState({
      products
    })
  }

  render() {
    return (
      <React.Fragment>
        {this.state.products.map((item, i) => <ListedIten key={i} data={item} />)}
      </React.Fragment>
    );
  }
}
