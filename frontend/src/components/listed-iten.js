import React from 'react';
import styled from 'styled-components';

const Styles = styled.div`
   .container {
      background-color: rgb(254, 253, 253);
      border: 1px solid rgb(237, 234, 233);
      border-radius: 4px;
      width: 242px;
      float: left;
      margin: 8px;
      cursor: pointer;
   }

   .container:hover {
      background-color: rgb(237, 234, 233);
      border-color: rgb(142, 140, 163);
   }

   .image-container {
     position: relative;
   }

   .discount {
      position: absolute;
      background-color: rgb(179, 170, 163);
      color: #fff;
      top: 15px;
      font-size: 13px;
      padding: 3px;
      font-weight: bold;
   }

   .image {
      width: 242px;
   }

   .description-container {
      padding: 10px;
   }

   .description-container .title {
      font-size: 17px;
      font-weight: 500;
      color: #232323;
      margin: 10px 0px;
   }

   .description {
     color: rgb(88, 88, 90);
     font-size: 12px;
     margin: 20px 0px;
   }

   .label-price {
     color: rgb(88, 88, 90);
     margin-bottom: 15px;
   }

   .value-price {
     color: rgb(51, 51, 51);
   }
`;

export default class ListedIten extends React.Component {
  render() {
    const { data } = this.props;

    return (
      <Styles>
        <div className='container'>
          <div className='image-container'>
            <div className='discount'>Até {data.discount}% OFF</div>
            <img className='image' src={data.imageSrc}/>
          </div>
          <div className='description-container'>
            <div className='title'>{data.title}</div>
            <div className='description'>{data.description}</div>
            <div className='label-price'>A partir de
              <span className='value-price'>R${data.price}</span>
            </div>
          </div>
        </div>
      </Styles>
    );
  }
}
